/**********SIMPLE LINKED LIST*********/

#include <stdio.h>
#include <stdlib.h>
struct node
{
    int value;
    struct node* next;

};
struct node* start=NULL;
struct node* ptr;

void insertvalue()
{
    ptr=(struct node*)malloc(sizeof(struct node));
    printf("Enter Value: ");
    scanf("%d",&ptr->value);
    ptr->next=NULL;
    if(start==NULL)
    {
        start=ptr;
    }
    else
    {
        struct node* curr=start;

        while(curr->next!=NULL)
        {
            curr=curr->next;
        }
        curr->next=ptr;
    }
}
struct node* searchList(int val)
{
    struct node* curr=start;
    int flag=0;
    while(curr!=NULL)
    {
        if(curr->value==val)
        {
            flag++;
            break;

        }
        curr=curr->next;
        }
        if(flag>0)
        {
            printf("Value found\n");

        }
        else
        {
            printf("Value not found\n");

        }


}
void swap(struct node *p1,struct node *p2, struct node *start)
{
    struct node *prev=NULL;
    struct node *curr=start;
    struct node *temp;
    if(p1==start)
    {
        while(curr!=NULL)
        {
            if(curr->next==p2)
            {
                prev=curr;
            }
            curr=curr->next;
        }
        temp=p1->next;
        p1->next=p2->next;
        p2->next=temp;
        prev->next=p1;
        start=p2;
        return;

    }
    if(p2==start)
    {
        while(curr!=NULL)
        {
            if(curr->next==p1)
            {
                prev=curr;
            }
            curr=curr->next;
        }
        temp=p2->next;
        p2->next=p1->next;
        p1->next=temp;
        prev->next=p2;
        start=p1;
        return;
    }
    temp=p1;
    while(curr!=NULL)
    {
        if(curr->next==p1)
        {
            prev=curr;
        }
        curr=curr->next;
    }
    p1->next=p2->next;
    prev->next=p2;
    p2->next=temp;

}
void deletee(int val)
{
    struct node* curr=start;
    if(start->value==val)
    {
        start=start->next;
        free(curr);
    }
    else
    {
        struct node* prev=start;
        curr=start->next;
        while(curr->value!=val)
        {
            curr=curr->next;
            prev=prev->next;
        }
        prev->next=curr->next;
        free(curr);
    }
}
void printlist()
{
    struct node* ptr=start;
    while(ptr!=NULL)
    {
        printf("%d\n",ptr->value);
        ptr=ptr->next;
    }
}
void push(struct node *start)
{
    if(start==NULL)
    {
        ptr=(struct node* )malloc(sizeof(struct node));
        printf("Enter push value: ");
        scanf("%d",&ptr->value);
        ptr->next=NULL;
        start=ptr;

    }
    else
    {
        struct node *curr=start;
        ptr=(struct node*)malloc(sizeof(struct node));
        //ptr->next=NULL;
        start=ptr;
        start->next=curr;
    }
}
void pop()
{
    struct node *curr=start;
    struct node *ptr;
    ptr=start->next;
    start=ptr;
    free(curr);

}

int main()
{

    int n,ch;
    int va;
    struct node *p1=NULL,*p2=NULL;
    int s1, s2;
    printf("\nHow many Numbers in the List? ");
    scanf("%d",&n);
    for(int i=0;i<n;i++)
    {
        insertvalue();
    }
    printlist();
    printf("\nNumber to search: ");
    scanf("%d",&ch);
    searchList(ch);
    printf("Number to delete? ");
    scanf("%d",&va);
    deletee(va);
    printlist();
    printf("\nSWAPPING\n");
    printf("Enter first number to swap: ");
    scanf("%d",&s1);
    p1=searchList(s1);
    printf("Enter second number to swap: ");
    scanf("%d",&s2);
    p2=searchList(s2);
    swap(p1,p2,start);
    printlist();


}
